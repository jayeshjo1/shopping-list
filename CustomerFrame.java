package heb;

import javax.swing.JFrame;
import javax.swing.event.EventListenerList;

public class CustomerFrame extends JFrame{

    private EventListenerList listenerList = new EventListenerList();
    private static final long serialVersionUID = 1L;
    CustomerFrame(String tittle)
    {
	super(tittle);
    }
    
	/**
	 * Fire Events for Main Menu
	 * @param event
	 */
	public void fireCustomerEvents(CustomerEvent event)
	{
		Object[] listeners = listenerList.getListenerList();
		
		for(int i = 0; i < listeners.length; i += 2)
		{
			if(listeners[i] == CustomerListener.class)
			{
				((CustomerListener)listeners[i+1]).customerEventOccurred(event);
			}
		}
	}
	/**
	 * add listener to main menu's listener list
	 * @param listener
	 */
	public void addCustomerListener(CustomerListener listener)
	{
		listenerList.add(CustomerListener.class, listener);
	}
	
	/**
	 * delete listener to main menu's listener list
	 * @param listener
	 */
	public void deleteCustomerListener(CustomerListener listener)
	{
		listenerList.remove(CustomerListener.class, listener);
	}
}
