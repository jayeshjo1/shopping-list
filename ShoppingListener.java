package heb;

import java.util.EventListener;

public interface ShoppingListener extends EventListener {
	
	/**
	 * MainMenuListener(MainMenuFrame event)
	 * 
	 * Allows external files to listen into any events fired off by Game Frames.
	 * 
	 * preconditions: only by MainMenu Frame and MainMenu Frame components.
	 * 
	 * @param event: event fired by a MainMenuFrame which can be accessed by external files.
	 * 
	 */
	public void shoppingEventOccurred(ShoppingEvent event);
}
