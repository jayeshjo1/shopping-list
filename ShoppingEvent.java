package heb;

import java.util.EventObject;

/** MainMenuEvent
 * 
 * Event created when it needs to be fired by MainMenu Frame.
 * @author Jayesh Joshi and Margret Tumbokon
 * @version 2.5
 * @see tutorial for Swing
 * 
 */


public class ShoppingEvent extends EventObject
{
	private static final long serialVersionUID = 1L;
	private boolean flag;
	
	/**
	 * Main Menu Event constructor uses its source and what button was pressed to create event
	 * @param source
	 * @param input
	 */
	public ShoppingEvent(Object source, boolean input) 
	{
		super(source);
		flag = input;
		
	}
	
	/** getFlag()
	 * Returns the flag within the Event.
	 * 
	 * @return flag : @see above comment for how it is set.
	 */
	public boolean getFlag()
	{
		return flag;
	}
}