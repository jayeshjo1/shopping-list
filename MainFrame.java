package heb;

import java.awt.BorderLayout;
import java.awt.Container;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class MainFrame extends JFrame implements MainMenuListener{
	private static final long serialVersionUID = 1L;
	private ArrayList<String> customers;
	private MainMenuPanel mainMenu;
	private ShoppingFrame sframe;
	private CustomerFrame cframe;
	MainFrame(String title)
	{
		super(title);
		customers = new ArrayList<String>();
		initializeFrame();
		setProperties();
		
		mainMenu = new MainMenuPanel();
		mainMenu.addMainMenuListener(this);
		
		Container c = getContentPane();
		
		c.add(mainMenu, BorderLayout.CENTER);
	}
	private void initializeFrame()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(640, 700);
		setResizable(false);
		setVisible(true);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
	}
	
	public void setProperties()
	{
		Properties properties = new Properties();
		String propFileName = "src/heb/HEB.properties";
		// try to find the file
		try
		{
			File file = new File(propFileName);
			FileInputStream fileInput = new FileInputStream(file);
			properties.load(fileInput);
			fileInput.close();
		}
		catch (FileNotFoundException e) 
		{
			// case: not found, create a new one 
			try
			{
				// initialize properties
				properties.setProperty("numCustomers", "0");
				
				// store properties
				File file = new File(propFileName);
				FileOutputStream fileOut = new FileOutputStream(file);
				properties.store(fileOut, "HEB Properties");
				fileOut.close();
			}
			catch (FileNotFoundException exception)
			{
				exception.printStackTrace();
			}
			catch (IOException exception)
			{
				exception.printStackTrace();
			}
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		// retrieve the keys and set global variables
		String test = properties.getProperty("numCustomers");
		int numCustomers = Integer.parseInt(test);
		for(int k = 0; k < numCustomers; k +=1)
		{
			customers.add(properties.getProperty(Integer.toString(k)));
		}
	}
	public void mainEventOccurred(MainMenuEvent event) {
		if(event.getFlag() == 0)
		{
			setVisible(false);
			sframe = new ShoppingFrame("Shopping List");
			sframe.addShoppingListener(new ShoppingListener()
			{
				public void shoppingEventOccurred(ShoppingEvent event)
				{
					// case: restart
					if(event.getFlag())
					{
						sframe.dispose();
						sframe = new ShoppingFrame("Shopping List");
						sframe.addShoppingListener(this);
					}
					// case: main menu
					else
					{
						sframe.dispose();
						setVisible(true);
					}
				}

			});
		}
		else if (event.getFlag() == 1)
		{
		    setVisible(false);
			cframe = new CustomerFrame("Add/Remove Customers");
			cframe.addCustomerListener(new CustomerListener()
			{
				public void customerEventOccurred(CustomerEvent event)
				{
					// exit
					cframe.dispose();
					setVisible(true);
				}
				
			});
		}
	}
	public static void main(String args[]) 
	{
	    SwingUtilities.invokeLater(new Runnable()
	    {
		public void run() 
		{
		    runShoppingList();
		}
		});
	}
		
	/**
	 * runs the Game by creating MainFrame
	 * @see Swing tutorials
	 */
	public static void runShoppingList()
	{
		
	    try
	    {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } catch (Exception e)
	    {
		e.printStackTrace();
	    }
	   
	    JFrame frame = new MainFrame("Shopping List");
	    
	}
}
