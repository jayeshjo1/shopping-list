package heb;

import javax.swing.JFrame;
import javax.swing.event.EventListenerList;

public class ShoppingFrame extends JFrame{

    private EventListenerList listenerList = new EventListenerList();
    private static final long serialVersionUID = 1L;
    ShoppingFrame(String tittle)
    {
	super(tittle);
    }
    
	/**
	 * Fire Events for Main Menu
	 * @param event
	 */
	public void fireShoppingEvents(ShoppingEvent event)
	{
		Object[] listeners = listenerList.getListenerList();
		
		for(int i = 0; i < listeners.length; i += 2)
		{
			if(listeners[i] == ShoppingListener.class)
			{
				((ShoppingListener)listeners[i+1]).shoppingEventOccurred(event);
			}
		}
	}
	/**
	 * add listener to main menu's listener list
	 * @param listener
	 */
	public void addShoppingListener(ShoppingListener listener)
	{
		listenerList.add(ShoppingListener.class, listener);
	}
	
	/**
	 * delete listener to main menu's listener list
	 * @param listener
	 */
	public void deleteShoppingListener(ShoppingListener listener)
	{
		listenerList.remove(ShoppingListener.class, listener);
	}
}
