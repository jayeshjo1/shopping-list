package heb;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class MainMenuPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	MainMenuPanel()
	{
	    	setSize(640, 700);
		Dimension size;
		JPanel menuPanel = new JPanel();
		menuPanel.setBackground(new java.awt.Color(25,25,112));
		
		//create How To Play Button;
		JButton addRemoveButton = new JButton ("Add/Remove Customers");
		size = addRemoveButton.getPreferredSize();
		addRemoveButton.addActionListener(new ActionListener()
		{	
			public void actionPerformed(ActionEvent e)
			{
				fireMainMenuEvents(new MainMenuEvent(this,1));
			}
			
		});
		
		// create New Game Button
		JButton startBt = new JButton("Start");
		startBt.setPreferredSize(size);
		startBt.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				fireMainMenuEvents(new MainMenuEvent(this,0));	
			}
		});
		
		// create Exit Button
		JButton exitBt = new JButton("Exit");
		exitBt.setPreferredSize(size);
		exitBt.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.exit(1);
			}
		});
			
		
		// put the buttons onto the panel
		menuPanel.setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.anchor = GridBagConstraints.CENTER;
		gc.insets = new Insets(0,70,0,0);
		gc.ipady = 10;
		gc.weightx = 0;
		gc.weighty = 5;
		gc.gridx = 0;
		gc.gridy = 0;
		menuPanel.add(startBt,gc);
		
		gc.gridx = 0; 
		gc.gridy = 1;
		menuPanel.add(addRemoveButton,gc);
		
		gc.gridx = 0; 
		gc.gridy = 2;
		menuPanel.add(exitBt,gc);
		

		// put panel on bottom so that image can go on top
		add(menuPanel, BorderLayout.CENTER);
	}
	/**
	 * Fire Events for Main Menu
	 * @param event
	 */
	public void fireMainMenuEvents(MainMenuEvent event)
	{
		Object[] listeners = listenerList.getListenerList();
		
		for(int i = 0; i < listeners.length; i += 2)
		{
			if(listeners[i] == MainMenuListener.class)
			{
				((MainMenuListener)listeners[i+1]).mainEventOccurred(event);
			}
		}
	}
	/**
	 * add listener to main menu's listener list
	 * @param listener
	 */
	public void addMainMenuListener(MainMenuListener listener)
	{
		listenerList.add(MainMenuListener.class, listener);
	}
	
	/**
	 * delete listener to main menu's listener list
	 * @param listener
	 */
	public void deleteMainMenuListener(MainMenuListener listener)
	{
		listenerList.remove(MainMenuListener.class, listener);
	}
}
